import {Component} from '@angular/core'
import {PostsService} from '../services/posts.service'

@Component({
	moduleId: module.id,
    selector: 'user',
    templateUrl: 'user.component.html',
    providers:[PostsService]
})
export class UserComponent { 
	name : string;
	email : string;
	address : address;
	hobbies : string[];
	showHobbies : boolean;
	posts : post[];

	constructor(private postsService:PostsService){
		this.name = 'John Doe';
		this.email = 'john@example.com'
		this.address = {
			street : '12 Main St.',
			city : 'Boston',
			state : 'MA'
		}	
		this.hobbies = ['Music','Movies','Sports'],
		this.showHobbies = false;

		this.postsService.getPosts().subscribe(posts => {
			this.posts = posts;
		})
	}

	toggltHobbies(){
		this.showHobbies = this.showHobbies !== true;
	}

	addHobby(hobby:string){
		this.hobbies.push(hobby);
	}

	deleteHobby(i:number){
		this.hobbies.splice(i,1);
	}
}

interface address{
	street : string;
	city : string;
	state : string;
}

interface post{
	id: number;
	title: string;
	body: string;
}